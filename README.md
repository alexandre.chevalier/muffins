# Muffins · ![](https://img.shields.io/badge/version-6.0-blue.svg) ![](https://img.shields.io/badge/goût-savoureux-ff69b4.svg) ![](https://img.shields.io/badge/qualité-incroyable-green.svg)

![](https://media.discordapp.net/attachments/370315577102827523/532660554733715456/Muffinz_noadkoko.jpg?width=224&height=168)
![](https://media.discordapp.net/attachments/370315577102827523/532660560307945473/Muffinz_frambouaz.jpg?width=224&height=168)

Comment faire des muffins savoureux.
1. [Préparation](#préparation)
1. [Framboises + chocolat blanc](#framboises-chocolat-blanc)
1. [Noix de coco + pépites de chocolat](#noix-de-coco-pépites-de-chocolat)

## Préparation

> ⏲️ ~**15** min. de préparation <div/>
> ⏲️ ~**20** min. de cuisson

> 🌡️  Préchauffer le four à 180°C

##### 1. Les poudres dans un *premier* bol
 1. Mélanger la farine, le sucre, la levure et le sel 
##### 2. Les liquides dans un *deuxième* bol
 1. Casser les oeufs et les battre
 1. Rajouter le lait et l'huile
##### 3. Mélanger les deux préparations
 1. Mélanger juste pour (à peine) homogénéiser
 1. Ne pas trop mélanger ni trop travailler la pâte
##### 4. Rajouter les ingrédients de fin (framboises, pépites, etc.)
##### 5. Disposer dans les moules
##### 6. Cuire au four pendant au moins 20 minutes

⚠️ Note : ne surtout pas arrêter la cuisson durant les 10 premières minutes.
___

## Framboises + chocolat blanc
Pour **9** muffins.
### Poudres
* 230 g farine
* 100 g sucre
* 1 sachet sucre vanillé
* 1/2 sachet de levure
* 1 pincée de sel
### Liquides
* 2 oeufs 
* 40 ml huile
* 120 ml lait
### À ajouter à la fin 
* 110 g framboises surgelées
* 64 g chocolat blanc en ¼ de carrés
___

## Noix de coco + pépites de chocolat
Pour **9** muffins.
### Poudres
* 230 g farine
* 100 g sucre
* 1 sachet sucre vanillé
* 1/2 sachet de levure
* 1 pincée de sel
* 80 g noix de coco en poudre
### Liquides
* 2 oeufs 
* 50 ml huile
* 120 ml lait

### À ajouter à la fin
* 100 g pépites de chocolat